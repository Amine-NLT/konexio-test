const Koa = require("koa");
const koaRouter = require("koa-router");
const koaBody = require("koa-body");
const router = new koaRouter();
const logger = require('koa-logger');
const cors = require('koa2-cors');
const port = process.env.PORT || 3000;
const mongo = require('koa-mongo')
const dbConfig = require('./dbconfig')

const app = new Koa();

app.use(mongo(dbConfig));
app.use(logger());
app.use(cors({
    origin: '*',
    allowMethods: ['GET', 'POST', 'DELETE', 'HEAD'],
    allowHeaders: '*',
  }));
app.use(koaBody(
    { 
        multipart: true,
        urlencoded: true
 }));

router.post("/register", async ctx => {
    const data = ctx.request.body;
    const result = await ctx.db.collection('users').insertOne(data)
    ctx.body = {status : "success"};
  });

router.get('/listUsers', async ctx => {
    ctx.body = await ctx.db.collection('users').find().toArray();
    
})

router.get('/user/:id', async ctx => {
    ctx.body = await ctx.db.collection('users').findOne({_id: ctx.params.id})
})

app.use(router.routes());
app.use(router.allowedMethods());

// Error handling
app.use(async (ctx, next) => {
    try {
        await next()
    } catch (err) {
        console.log(err.status)
        ctx.status = err.status || 500;
        ctx.body = err.message;
    }
})

app.listen(port, () => console.log("Koa server listening on port " + port));
