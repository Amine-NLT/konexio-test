const dbConfig = {
    host: 'database',
    port: 27017,
    user: 'konexio',
    pass: 'test',
    db: 'konexio',
    authSource: 'admin',
    max: 100,
    min: 1
  }

  module.exports = dbConfig