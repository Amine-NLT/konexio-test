import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import Register from "./Register";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavbarText,

} from 'reactstrap';
import Users from "./Users";
import User from "./User";

export default function AppRouter() {
  return (
    <Router>
      <div>
      <div>
      <Navbar color="dark" dark expand="md">
        <NavbarBrand href="/">Konexio Test</NavbarBrand>
        <NavbarToggler />
        <Collapse isOpen={false} navbar>
          <Nav className="mr-auto" navbar>
          </Nav>
          <NavbarText>Amine Mohamadi</NavbarText>
        </Collapse>
      </Navbar>
    </div>
        <Switch>
        <Route path="/user/:id">
        <User />
      </Route>
          <Route path="/register">
            <Register />
          </Route>
          <Route path="/">
          <Users />
        </Route>
        </Switch>
      </div>
    </Router>
  );
}
