import React, { Component } from 'react';
import './App.css';
import { Card, CardBody, CardTitle, CardText, CardImg } from 'reactstrap';

class Users extends Component {

  constructor(props) {
    super(props)
    this.state = {
        users : []
    }
  }

  componentDidMount() {
    fetch("http://localhost:3000/listUsers")
    .then(response => response.json())
    .then(data => this.setState({users : data}));
  }

  render() {
      console.log(this.state.users)
    return (
      <div>
      {this.state.users.map((user) => 
        <Card>
        <CardImg top width="100%" src={user.picture} alt="Card image cap" />
        <CardBody>
          <CardTitle>{`${user.firstname} ${user.lastname}`}</CardTitle>
          <CardText><small className="text-muted">{user.email} - {user.status}</small></CardText>
        </CardBody>
      </Card>
        )}
      </div>
    )
  }
}

export default Users;
