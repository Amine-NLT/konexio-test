import React, { Component } from 'react';
import './App.css';
import {
  Label,
  FormGroup,
  Form,
  Button,
  Input,
} from 'reactstrap';
import { withRouter } from "react-router-dom";

class User extends Component {

  constructor(props) {
    super(props)
    this.state = {
        user : {}
    }
  }

  componentDidMount() {
    console.log(this.props.match.params.id);
    fetch(`http://localhost:3000/user/${this.props.match.params.id}`)
    .then(response => response.json())
    .then(data => this.setState({user : data}));
  }

  render() {
    return (
      <div>
      <Form>
      <FormGroup>
        <Label for="firstName">Firstname</Label>
        <Input type="text" name="firstname" value={this.state.user.firstname} id="firstname" placeholder="Georges" required />
      </FormGroup>
      <FormGroup>
        <Label for="lastName">Lastname</Label>
        <Input type="text" name="lastname" value={this.state.user.lastname} id="lastname" placeholder="Doe" />
      </FormGroup>
      <FormGroup>
        <Label for="email">Email</Label>
        <Input type="email" name="email" value={this.state.user.email} id="email" placeholder="test@example.io" required />
      </FormGroup>
      <br></br>
      <Button>Update</Button>
    </Form>
      </div>
    )
  }
}

export default withRouter(User);