import React, { Component } from 'react';
import './App.css';
import {
  Label,
  FormGroup,
  Form,
  Button,
  Input,
} from 'reactstrap';
import S3 from 'react-aws-s3';
import config from './S3Config';
import { v4 as uuidv4 } from 'uuid';

class Register extends Component {

  constructor(props) {
    super(props)
    this.state = {
      newsletter: false,
      terms: false
    }
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const { password, passwordConfirm } = this.state;
    if (password !== passwordConfirm) {
      alert("Passwords don't match");
    } else {
      const ReactS3Client = new S3(config);
      const uuid = uuidv4();
      ReactS3Client
        .uploadFile(this.state.picture, uuid)
        .then(data => this.sendToAPI(data, uuid))
        .catch(err => console.error(err))
    }
  }

  sendToAPI = (AS3Data, uuid) => {
    var data = this.state;

    data.picture = AS3Data.location;
    data._id = uuid;
    delete data.passwordConfirm;

    var result = async () => await fetch("http://localhost:3000/register", {
      method: 'POST',
      body: JSON.stringify(data)
    }
    )
  }

  onChange = (e) => {
    if (e.target.type === "checkbox") {
      this.setState({ [e.target.name]: !this.state[e.target.name] })
    } else if (e.target.type === "file") {
      this.setState({ [e.target.name]: e.target.files[0] })
    } else {
      this.setState({ [e.target.name]: e.target.value })
    }
  }

  render() {
    return (
      <div>
        <div>
          <Form onSubmit={this.handleSubmit} onChange={this.onChange}>
            <FormGroup>
              <Label for="firstName">Firstname</Label>
              <Input type="text" name="firstname" id="firstname" placeholder="Georges" required />
            </FormGroup>
            <FormGroup>
              <Label for="lastName">Lastname</Label>
              <Input type="text" name="lastname" id="lastname" placeholder="Doe" />
            </FormGroup>
            <FormGroup>
              <Label for="email">Email</Label>
              <Input type="email" name="email" id="email" placeholder="test@example.io" required />
            </FormGroup>
            <FormGroup>
              <Label for="password">Password</Label>
              <Input type="password" name="password" id="password" placeholder="password" required />
            </FormGroup>
            <FormGroup>
              <Label for="passwordConfirm">Password confirmation</Label>
              <Input type="password" name="passwordConfirm" id="passwordConfirm" placeholder="password" required />
            </FormGroup>
            <FormGroup>
              <Label for="exampleSelect">Status</Label>
              <Input type="select" name="status" id="status">
                <option></option>
                <option>Teacher</option>
                <option>Teacher assistant</option>
                <option>Student</option>
              </Input>
            </FormGroup>
            <FormGroup>
              <Label for="picture">Profile picture</Label>
              <Input type="file" name="picture" id="picture" required />
            </FormGroup>
            <FormGroup check>
              <Label check>
                <Input type="checkbox" name="newsletter" id="newsletter" />{' '}
          Subscribe to the newsletter
        </Label>
            </FormGroup>
            <FormGroup check>
              <Label check>
                <Input type="checkbox" name="terms" id="terms" required />{' '}
        I have read terms and conditions
      </Label>
            </FormGroup>
            <br></br>
            <Button type="submit">Submit</Button>
          </Form>
        </div>
      </div>
    )
  }
}

export default Register;
