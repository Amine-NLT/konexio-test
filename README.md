# Konexio Test

Cette application est constituée de trois parties : 
- Un back-end en Node.JS avec la librairie Koa
- Un front-end en ReactJS
- Une base de données MongoDB

# Principe de base
L'application permet de créer des utilisateurs, de les lister et les modifier.
Elle dispose de 3 pages : 
- "**/**" &rarr; Sur laquelle on peut voir la liste des utilisateurs existants et leur photo
- "**/register**" &rarr; Qui donne accès au formulaire permettant de créer un nouvel utilisateur
- "**/user:id**" &rarr; Qui permet d'accéder à un utilisateur via son Id et le modifier


## Stockage

Les données utilisateurs sont stockées dans une base de données NoSQL MongoDB.
Les photos de profil sont stockées dans un bucket Amazon S3.

## Lancement de l'application

L'application intègre **docker-compose** qui permet de lancer simultanément sur une machine l'API, le front-end et la base de données. Il faut se trouver sur une machine (Unix ou Windows) et lancer la commande suivante :
```
docker-compose up --build
```
Le front est alors accessible à l'URL suivante : http://localhost/ et l'API via l'URL suivante : http://localhost:3000

